import argparse
import discord
import onnx
import onnxruntime
import cleantext
import numpy
import math
from enum import Enum
from transformers import DistilBertTokenizer


class Toxicity(Enum):
	NORMAL = 0
	TOXIC = 1
	SEVERE_TOXIC = 2
	OBSCENE = 3
	THREAT = 4
	INSULT = 5
	IDENTITY_HATE = 6


def run_bot(token, fn_classify_message, fn_handle_toxicity):
	intents = discord.Intents.default()
	intents.message_content = True
	client = discord.Client(intents = intents)

	tokenizer = DistilBertTokenizer.from_pretrained("distilbert-base-uncased")
	model = onnx.load("model.onnx")
	session = onnxruntime.InferenceSession(model.SerializeToString())

	@client.event
	async def on_message(message):
		if message.author == client.user:
			return
		content = str(message.content)
		toxicity = await fn_classify_message(session, tokenizer, content)
		await fn_handle_toxicity(message, toxicity)

	client.run(token)


def sigmoid(x):
  return 1 / (1 + math.exp(-x))

sigmoid_v = numpy.vectorize(sigmoid)

async def classify_message(session, tokenizer, message, threshold = 0.5):
	message = cleantext.clean(message, extra_spaces = True, lowercase = True, numbers = True, punct = True)
	tokens = tokenizer(message, padding = "max_length", truncation = True, return_tensors = "np")
	result = session.run(["logits"], input_feed=dict(tokens))
	prediction = sigmoid_v(result[0][0])
	indices = (prediction >= threshold).nonzero()[0]
	toxicities = [Toxicity(1 + index) for index in indices]
	print(message, prediction, toxicities)
	return toxicities


async def handle_toxicity(message, toxicities):
	if not toxicities:
		await message.add_reaction("😊")
		return
	if Toxicity.TOXIC in toxicities:
		await message.add_reaction("😢")
	if Toxicity.SEVERE_TOXIC in toxicities:
		await message.add_reaction("😠")
	if Toxicity.OBSCENE in toxicities:
		await message.add_reaction("😬")
	if Toxicity.THREAT in toxicities:
		await message.add_reaction("😰")
	if Toxicity.INSULT in toxicities:
		await message.add_reaction("😒")
	if Toxicity.IDENTITY_HATE in toxicities:
		await message.add_reaction("😡")


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Run the anti toxicity responder bot!')
	parser.add_argument('token', help='Discord API token')
	args = parser.parse_args()
	run_bot(args.token, classify_message, handle_toxicity)
